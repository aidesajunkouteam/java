/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package location;

import java.util.List;

/**
 *
 * @author fgjn
 */
public class Immeuble {
    // Déclaration des propriétés

    private int code;
    private String adresse;
    private Piece[] pieces;
    private float prixLouer;
    private float prixCharge;

    /**
     * Constructeur
     *
     * @param code
     * @param adresse
     * @param prixLouer
     * @param prixCharge
     * @param nbPiece
     */
    public Immeuble(int code, String adresse, float prixLouer, float prixCharge, int nbPiece) {
        this.code = code;
        this.adresse = adresse;
        this.prixLouer = prixLouer;
        this.prixCharge = prixCharge;
        // initialisationdu tableau de pieces
        pieces = new Piece[nbPiece];
    }
    
    

}
