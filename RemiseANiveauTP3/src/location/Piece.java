/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package location;

/**
 * Piece d'un Immeuble
 *
 * @author fgjn
 */
public class Piece {

    /**
     * Description de la piece
     */
    private String descrition;
    /**
     * Surface en metre carre
     */
    private float surface;

    /**
     * Constructeur pour initialiser les 2 parametres
     *
     * @param descrition
     * @param surface
     */
    public Piece(String descrition, float surface) {
        this.descrition = descrition;
        this.surface = surface;
    }

    /**
     * Costructeur ne prenant qu'un seul param. on utilise le constructeur a 2
     * param via la methode this pour reunir les test et les traitements si
     * besoin
     *
     * @param descrition
     */
    public Piece(String descrition) {
        this(descrition, 0);
    }

    public String getDescrition() {
        return descrition;
    }

    public void setDescrition(String descrition) {
        this.descrition = descrition;
    }

    public float getSurface() {
        return surface;
    }

    public void setSurface(float surface) {
        this.surface = surface;
    }

    @Override
    public String toString() {
        return "La piece = " + descrition + " contient " + surface + "m2";
    }
    
    

}
