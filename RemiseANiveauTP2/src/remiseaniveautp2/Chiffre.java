/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp2;

/**
 *
 * @author fgjn
 */
public class Chiffre {

    private char alphabet[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

    /**
     * Méthode de chiffrement.<p>
     * on part sur le predicat que : <br>
     * - Les char sont en fait de code ASCII. le a = 97<br>
     * - Les tabelaux sont indexés de 0 à n<br>
     * - les String sont des chaines de caractere et on peut les parcourir
     * élément par élément<br>
     *
     * @param chaine chaine à chiffrer
     * @return chaine chiffrée
     */
    public String chiffrage(String chaine) {
        String out = "";

        // on boucle sur les char de la chaine 
        for (char c : chaine.toCharArray()) {
            // calcul de l'indice du caractère en cours.
            int id = ((int) c) - 97;
            // on decale de 2
            id += 2;
            // on depasse la taille du tableau on repart au debut;
            int nbTours = 0;
            while (id >= alphabet.length) {
                id = id - alphabet.length;
                nbTours++;
            }
            // on ajoute le caractere dans le tableau de sortie
            out += alphabet[id];
        }

        return out;
    }

    /**
     * Meme prédicat que pour le chiffrage
     *
     * @param chaine chaine a déchiffrer
     * @return chaine déchiffré
     */
    public String dechiffrage(String chaine) {
        String out = "";
        for (char c : chaine.toCharArray()) {
            // calcul de l'indice du caractère en cours.
            int id = ((int) c) - 97;
            // on decale de 2
            id -= 2;
            // on depasse la taille du tableau on repart au debut;
            int nbTours = 0;
            while (id < 0) {
                id = id + alphabet.length;
                nbTours++;
            }
            // on ajoute le caractere dans le tableau de sortie
            out += alphabet[id];
        }

        return out;
    }
}
