/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp2;

/**
 * Ce TP est ceui de cryptographie Simple
 * @author fgjn
 */
public class RemiseANiveauTP2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Demande de chiffrement avec affichage
        System.out.println(new Chiffre().chiffrage("abcdefgh"));
        // demande de dechiffrement avec affichage 
        System.out.println(new Chiffre().dechiffrage("cdefghab"));
    }
    
}
