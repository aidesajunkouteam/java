/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp1;

import remiseaniveautp1.employe.Employe;
import java.util.ArrayList;

/**
 * Classe Projet.<p>
 * Cette classe à pour objectif la gestion des projets de l'entreprise.<br>
 * La classe va définir un Projet. L instentiation de cette classe lors de
 * l'appel par un new Projet(...) permettra la construction d'une instance en
 * mémoire d un projet et donc d'un Objet Projet.
 *
 * @author fgjn
 */
public class Projet {

    // Les propriétés sont marqués private afin de les rendre non visible par
    // les objets qui utiliseront l'objet Projet
    // nom du projet
    private String nom;
    // début du projet
    private String dateDebut;
    // fin du projet
    private String dateFin;
    // Liste des Employes du projet (ArrayList et un Objet qui permet de gérer 
    //facilement des listes d'Objets ici des Employe)
    private ArrayList<Employe> employes;
    // Liste des Taches
    private ArrayList<Tache> taches;

    /**
     * Premier constructeur demandé par l'exercice.
     * <p>
     * C est grace aux constructeur que l'on peut créer à partir des classe des
     * objets.<p>
     * Ici Le constructeur s'utilisera en lui passant es arguments nom,
     * dateDebut et dateFin dela maniere suivante .
     * <pre>
     * .... Suite d'instruction du code....
     * Projet monProjetUn = new Projet("Nom Du Projet Un","23/11/2016","23/11/2017");
     * .... instructions du reste du code....
     * </pre>
     *
     * Ici l'énumeration des parametres qui doivent être passés à ce
     * constructeur. Lesparametre sont passé entre les parenthèse d'une
     * méthode.<p>
     * Le constructeur n'a JAMAIS de valeur de retour.
     *
     * @param nom Permet de définirle nom du projet
     * @param dateDebut Permet de définir la date de début d projet
     * @param dateFin Permet de définir la date de fin du projet
     */
    public Projet(String nom, String dateDebut, String dateFin) {
        // Rappel : `this` permet de signaler que l'on se refaire à l'objet 
        // en cours.
        // Par exemple, ici il y a 2 variables 'nom'. 
        // Celle qui est passée en paramêtre entre les parenthèses et 
        // Celle définit plus haut qui est l'attribut de la 
        // classe projet.
        // il en va de même pour les autres variables.
        // this va permettre de différencier ces 2 variables.
        // Ici on attribut à la variable om du Projet, en cours d'instentiation, 
        // La valeur de la variable du nom pasé en paramêtre.

        this.nom = nom;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    /**
     * Cette méthode permet d'ajouter un Employer à la liste du projet. Elle
     * attend en paramêtre d'entrée un Employe
     *
     * @param emp Employe qu'il va falloir ajouter à la liste.
     */
    public void ajouterEmployer(Employe emp) {
        // Ici on vérifie que la liste des employes a été construite.
        // en effet l'ArrayList est une classe qui permet d'instancier un 
        // Objet dont le travail est de gérer des listes
        if (employes == null) {
            // Si employes est null (qu'il n'a pas été créé)
            // alors on fait un new afin d'avoir une instance d'ArrayList qui 
            // s'occupera des Employe.
            employes = new ArrayList<>();
        }
        // ici on ajoute un employé dans la liste.
        // l'Objet ArrayList possède une méthode pour ajouter des élements typés 
        // lors de sa définition, ici des Employe.
        // Pour avoir une idée des méthodes d'un objet, ici ArrayList,
        // il suffit, arès avoir tapé le nom de la variable d'ajouter un '.'
        // une liste apparait avec toutes les méthodes disponnible pour cette 
        // classe.
        employes.add(emp);

    }

    /**
     * Cette classe fonctionne comme celle pour ajouter un Employe mais pour les
     * Classe Tache
     *
     * @param tache tache à prendre en compte
     */
    public void ajouterTache(Tache tache) {
        // vérification si la liste des taches existe. Sinon création de 
        // l'instance. Si taches est null, donc qu'elle pointe sur le vide, 
        // la liste n'existe pas en mémoire. null est un mot reservé 
        // qui veut dire qu'il n'y a rien, le vide. 
        if (taches == null) {
            // Création de la liste des taches.
            // l'appel du onstructeur se fait comme une méthode il ne faut 
            // donc pas oubler les ()
            taches = new ArrayList<>();
        }
        // Ajout de la tache à la liste
        taches.add(tache);
    }

    /**
     * Cette méthode permet de demander à l'objet de retourner des infos sous
     * forme de chaine de caractere.<p>
     * le @Override signifit que cette méthode est uen méthode qui est Hérité de
     * l'objet supérieur mais qu'on la surcharge. (on remplace son code par le
     * notre)
     * <p>
     * Rappel toutes les classes de Java hérite des propriétés et méthodes de la
     * classe Object.
     *
     * @return la chaine fabriquée.
     */
    @Override
    public String toString() {
        // le mot clef return permet de signifier ce que l'on doit retourner. 
        // après un return on sort de la méthode.
        // le \n permet un saut de ligne lors de l'affichage.
        //
        // creation d'une variable locale à la méthode (elle ne durera que 
        // le temps de l'execution la méthode puis sera detruite. elle sera 
        // recréée a chaque appel de la méthode.)
        // Cette variable va servir à fabriquer la chaien de sortie. 
        String out = "";
        out += "Projet:" + nom + ", date Debut:" + dateDebut + ", dateFin:" + dateFin;
        // ajout les informations des Employe via leurs méthodes ToString.
        // On parcours la liste des Employes si elle existe et on ajoute les informations à out
        if (employes != null) {
            out += "\n, employes:";
            for (Employe emp : employes) {
                out += "\n" + emp.toString();
            }
        }else{
            out+=", pas d'employé sur ce projet actuellement.";
        }
        // Pas demandé mais permet une lecture plus claire
         if (taches != null) {
            out += "\n, Taches:";
            for (Tache t : taches) {
                out += "\n" + t.toString();
            }
        }else{
            out+=", pas de tache sur ce projet actuellement.";
        }

        // On retourne out
        return out;
    }

}
