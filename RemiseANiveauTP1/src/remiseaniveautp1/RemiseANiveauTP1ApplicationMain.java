/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp1;

import remiseaniveautp1.employe.ChefProjet;
import remiseaniveautp1.employe.Developpeur;
import remiseaniveautp1.employe.Technicien;

/**
 * Classe principal de l'application.<p>
 * C'est par cette classe que le programme va s'executer en passant par la
 * méthode main()
 * <p>
 * Objectif du code :<br>
 * Créer une hiérarchie de classe pour gérer un projet d'une entreprise.
 *
 * @author fgjn
 */
public class RemiseANiveauTP1ApplicationMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Methode d'entrée dans le programme.

        System.out.println("Création du projet.");
        Projet projetAmazone = new Projet("Amazon", "23/11/2016", "23/11/2017");
        System.out.println("Projet Créé " + projetAmazone);

        System.out.println("Création du chef de projet");
        ChefProjet cp = new ChefProjet("Marques", "Olivier", 4500);
        System.out.println("Chef de projet créé : " + cp);
        System.out.println("Création du Technicien");
        Technicien tec = new Technicien("Agier", "Rapahel", 1900, "spécialité réseaux");
        System.out.println("Création du Technicien " + tec);
        System.out.println("Création du Developpeur");
        Developpeur dev = new Developpeur("Ficher", "Rapahel", 2500, new String[]{"Java", "Python"});
        System.out.println("Création du Developpeur " + dev);

        System.out.println("Ajout des employés au projet.");
        projetAmazone.ajouterEmployer(cp);
        projetAmazone.ajouterEmployer(tec);
        projetAmazone.ajouterEmployer(dev);
        System.out.println("Employés ajoutés " + projetAmazone);

        System.out.println("Création des taches");
        Tache t1 = new Tache("Configuration serveur web", 2, 0, null);
        Tache t2 = new Tache("programmer les achats", 20, 0, null);
        System.out.println("Taches créées" + t1 + t2);

        System.out.println("Affectation des taches");
        cp.affecterTache(t1, tec, projetAmazone);
        cp.affecterTache(t2, dev, projetAmazone);
        System.out.println("Taches affectées " + projetAmazone );
        
        System.out.println("Evolution de la tache du dev tache 2");
        for(int i = 0;i<3;i++){
            dev.executerTache(projetAmazone, t2);
        }
        System.out.println("Evolution de la tache du tec tache 1");
        for(int i = 0;i<1;i++){
            tec.executerTache(projetAmazone, t1);
        }
        
        System.out.println("Taches affectées " + projetAmazone );
        
        
        
        
        System.out.println("FIN DU PROGRAMME");

    }

}
