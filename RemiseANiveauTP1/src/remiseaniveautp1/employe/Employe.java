/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp1.employe;

/**
 * Classe Employe.<p>
 * Cette classe est la classe ancètre des autres classe employe du projet
 * (Developpeur, Technicien...) cf le schéma Entité/Association créé depuis
 * l'exercice.
 * .<p>
 * Elle permet de regrouper les informations/attributs/méthodes communes aux
 * classes qu hériteront
 * <p>
 * Elle doit être public afin d'être vu des autres packages de l'application.
 * exemple, utilisation dans la classe Projet
 *
 * @author fgjn
 */
public class Employe {

    // déclaration des propriétés de la classe employé.
    private int id;
    // Il y a une contraite sur la propriete id qui indique l'unicité 
    // de sa valeur
    // On va donc créer un compteur. elle sera pas stocke dans la classe pas 
    // partagé en mémoire par tous les objets afin que tous y accede.
    // se compteur (cpt) sera incrémenté a chaque fois qu'il sera utlisé.
    // pour signaler à java qu'une propriété est partagés on la préfix du mot 
    // static. 
    // Traduction de la ligne ci-dessous
    // static : veut dire partagé
    // private : visible uniquement par Employe 
    // int : type entier
    // cpt : nom de la variable
    // =0 : intialisation à 0 de la valeur à zéro.
    static private int cpt = 0;
    private String nom;
    private String prenom;
    // le type float est un type pour les nombre réel
    private float salaire;

    /**
     * Constructeur à 3 paramètres.<p>
     * Grâce à ce constructeur on peut initialiser chaque propriétés lors de
     * l'instanciation l'objet. On va utiliser les setter des propriétés afin
     * d'initialiser les valeurs. Cela va permettre d'appliquer les contraintes
     * à un seul endroit.
     *
     * @param nom
     * @param prenom
     * @param salaire
     */
    public Employe(String nom, String prenom, float salaire) {
        setNom(nom);
        setPrenom(prenom);
        setSalaire(salaire);
        // il ne faut pas oublier l'id
        setId();
    }

    /**
     * 2 eme constructeur à 2 paramètres qui permet d'initialiser le salaire à
     * 0.
     * <p>
     * On aura donc 2 manières de créer un un objet Employe.
     * <p>
     * Ce deuxième constructeur utilise le 1er en lui passant les valeurs nom et
     * prénom recu et en lui donnant 0 comme salaire. (Voir le Polymorphisme en
     * objet pour plus d'information)
     *
     * @param nom
     * @param prenom
     */
    public Employe(String nom, String prenom) {
        this(nom, prenom, 0);
    }

    /**
     * Permet de récupérer le numéro d'id de l'objet.
     *
     * @return le numero d'id
     */
    public int getId() {
        return id;
    }

    /**
     * Initialisation de l'id.
     * <p>
     * c'est ici que va être utilisé le compteur. le setId est mis en private
     * fin que personne d'autre ne puisse l'utiliser et incrémenter le compteur;
     */
    private void setId() {
        // on incrémete de 1
        cpt = cpt++; // équivaut à cpt = cpt + 1;
        // on affecte cpt a id
        this.id = cpt;
    }

    /**
     * Permet de récupérer le nom de l'employé
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet de mettre à jour le nom
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Permet de récupérer le prénom
     *
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Permet de mettre à jour le prénom
     *
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Permet de récupérer le salaire
     *
     * @return
     */
    public float getSalaire() {
        return salaire;
    }

    /**
     * Permet de mettre à pour le salaire
     *
     * @param salaire
     */
    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    /**
     * Cette méthode permet de demander à l'objet de retourner des infos sous
     * forme de chaine de caractère.<p>
     * le @Override signifit que cette méthode est une méthode qui est Héritée
     * de l'objet supérieur mais qu'on la surcharge. (on remplace son code par
     * le notre)
     * <p>
     * Rappel : toutes les classes de Java héritent des propriétés et méthodes
     * de la classe Object.
     *
     * @return la chaine fabriquée.
     */
    @Override
    public String toString() {
        return "Nom:" + nom + ", prénom:" + prenom + ", salaire:" + salaire;
    }

}
