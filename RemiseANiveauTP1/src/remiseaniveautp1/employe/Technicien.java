/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp1.employe;

import java.util.Random;
import java.util.stream.DoubleStream;
import remiseaniveautp1.Projet;
import remiseaniveautp1.Tache;

/**
 *
 * @author fgjn
 */
public class Technicien extends Employe {

    // definition des propriétés particulières a un technicien.
    private String specialite;

    /**
     * Constructeur de la classe. permet d'initialiser tous les parametres de la
     * classe. On utilise le constructeur de la classe mere pour les propriétés
     * communes.
     *
     * @param nom
     * @param prenom
     * @param salaire
     * @param specialite
     */
    public Technicien(String nom, String prenom, float salaire, String specialite) {
        super(nom, prenom, salaire);
        setSpecialite(specialite);
    }

    /**
     * Permet de récupérer la valeur de la spécialité
     *
     * @return
     */
    public String getSpecialite() {
        return specialite;
    }

    /**
     * permet d'initialiser la spécialité
     *
     * @param specialite
     */
    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    /**
     * Methode toString Question 6. point 5
     *
     * @return
     */
    @Override
    public String toString() {
        return super.toString() + ",statut: technicien";
    }

    /**
     * Permet l'augmentation de la completude, l'affichage du travail execute
     *
     * @param projet
     * @param tache
     * @return le projet
     */
    public Projet executerTache(Projet projet, Tache tache) {
        // recuperation et augmentation de la valeur de manière aléatoire
        // valeur a ajouter a la comletude.
        // on utilise un objet Random qui permet la creation de nombres pseudo 
        // aleatoire et deja typé (double, int....) voir les méthode proposé par
        // cette classe
        // on met a jour la completude. ici en 1 seule ligne. pour une facon pas
        // à pas voir la classe Developpeur
        tache.setCompletude(tache.getCompletude() + new Random().nextFloat());

        // affichage du résultat
        System.out.println("Moi, " + getNom() + " technicien, exécute la tache "
                + tache.getDescription() + ". Pourcentage du travail réalisé "
                + 100 * tache.getCompletude() + "%");
        return projet;
    }

}
