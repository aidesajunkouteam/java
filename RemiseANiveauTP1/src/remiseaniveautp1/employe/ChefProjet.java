/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp1.employe;

import remiseaniveautp1.Projet;
import remiseaniveautp1.Tache;

/**
 * Cette classe hérite de la classe Employe.
 * <p>
 * Cela va permettre de récupérer les propriétés et méthode dela classe Employe
 * dans la classe Chef de projet sans avoir à les réécrire.
 * <p>
 * pour cela on va employé le mot 'extends' pour signifier que l'on étend de la
 * classe
 *
 * @author fgjn
 */
public class ChefProjet extends Employe {

    /**
     * Ici le constructeur du chef de projet qui va permettre de l'initiliser en
     * appelant le constructeur de l'objet supérieur via le mot super.
     *
     * @param nom
     * @param prenom
     * @param salaire
     */
    public ChefProjet(String nom, String prenom, float salaire) {
        super(nom, prenom, salaire);
    }

    /**
     * Pour cette méthode toString on utilise la méthode d la classe Employe,
     * super.toString() et on lui ajoute la position
     *
     * @return
     */
    @Override
    public String toString() {
        return super.toString() + ",Position: Chef de Projet";
    }

    /**
     * Méthode pour affecter une tache
     * <p>
     * Question 4 point 3
     * .<p>
     * @param tache
     * @param emp
     * @param projet
     * @return
     */
    public Projet affecterTache(Tache tache, Employe emp, Projet projet) {
        // verification si l'employé est bien un Developpeur ou un technicien
        // pour vérifier si un  objet est bien définit par une classe.

        if(emp instanceof Developpeur || emp instanceof Technicien){
            // si oui alors
            // affecte la tache à l'emp
            tache.setEmploye(emp);
            // ajoute la teche au projet
            projet.ajouterTache(tache);
        }
        
        // retourner le projet
           return projet;
    }

}
