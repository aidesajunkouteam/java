/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp1.employe;

import remiseaniveautp1.Projet;
import remiseaniveautp1.Tache;

/**
 *
 * @author fgjn
 */
public class Developpeur extends Employe {

    // Définition des propriétés particulières  du Dev
    // definition du tableau de chaine de caractere qui contiendra les langages
    private String[] langages;

    /**
     * Ce constucteur fait appel au constructeur de la classe mère et met a jour
     * le tableau des langage du developpeur instancié.
     *
     * @param nom
     * @param prenom
     * @param salaire
     * @param langages
     */
    public Developpeur(String nom, String prenom, float salaire, String[] langages) {
        super(nom, prenom, salaire);
        setLangages(langages);
    }

    /**
     * Permet de récupérer le tableau des langages
     *
     * @return
     */
    public String[] getLangages() {
        return langages;
    }

    /**
     * Permet de remplir le tableau des langage a partir d'un tableau
     *
     * @param langages
     */
    public void setLangages(String[] langages) {
        this.langages = langages;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return super.toString() + ",position: Developpeur";
    }

    /**
     * Question 5. point 5
     * <p>
     * Méthode qui permet de faire exécuter une tache au Developpeur
     *
     * @param projet
     * @param tache
     */
    public void executerTache(Projet projet, Tache tache) {
        // on recupère la completude
        float comp = tache.getCompletude();
        // on augmente de 10%
        comp += 0.1;
        // on met à jour la completude
        tache.setCompletude(comp);
        //on affiche le texte
        // la variable nom n'est pas accessible car private. on passe donc
        // par son accesseur getNom()
        
        System.out.println("Moi, " + getNom() + 
                " développeur, exécute la tache " + tache.getDescription() 
                + ". Pourcentage du travail réalisé: " + 
                100 * tache.getCompletude()+"%");

    }

}
