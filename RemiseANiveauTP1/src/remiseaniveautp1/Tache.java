/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remiseaniveautp1;

import remiseaniveautp1.employe.Employe;

/**
 * Cette Classe va permettre de construire des Objets Tache.
 * <p>
 * L instentiation de cette classe lors de l'appel par un new Tache(...)
 * permettra la construction d'une instance en mémoire d une tache et donc d'un
 * Objet Tache.
 *
 * @author fgjn
 */
public class Tache {

    // Creation des propriétés de la classe Tache
    // description du projet
    private String description;
    // duree du projet en nombre de jours maximum de la tache.
    private int duree;
    // valeur d'avancé de la tache de 0(pas effectuée - 1 tache finie)
    private float completude;
    // Employe qui va effectuer la tache
    private Employe employe;

    /**
     * Constructeur pour instancie une tache avec toutes les valeurs en
     * paramètres pour initialiser les propriétés
     *
     * @param description
     * @param duree
     * @param completude
     * @param employe
     */
    public Tache(String description, int duree, float completude, Employe employe) {
        // afin de vérifier les valeur on laisse les methode de set se chaarger 
        // de la mie à jour des propriétés. cf. contraite completude 
        setDescription(description);
        setDuree(duree);
        setCompletude(completude);
        setEmploye(employe);
    }

    /**
     * Permet de récupérer la valeur de la description
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Permet d'initialiser la description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Permet de récupérer la durée
     *
     * @return
     */
    public int getDuree() {
        return duree;
    }

    /**
     * Permet d'initialiser la durée
     *
     * @param duree
     */
    public void setDuree(int duree) {
        this.duree = duree;
    }

    /**
     * permet de récupérer la complétude
     *
     * @return
     */
    public float getCompletude() {
        return completude;
    }

    /**
     * Permet d'initialiser la complétude.
     * <p>
     * le test d'excatitude de la valeur ce fait ici comme cela les
     * classes/objets n'ont pas à faire un test.
     *
     * @param completude
     */
    public void setCompletude(float completude) {
        // aplication de la contraite des limites dela comlétude.
        // L'objet ne se laissera pas mettre à jour si la valeur n'est pas correcte
        if (completude > 0 && completude < 1) {
            this.completude = completude;
        } else if (completude > 1) { // pour faire propre on va concidérer qu'au dessus de 1 c'est 1
            this.completude = 1;
        } else { // et que en dessous de 0 c'est 0
            this.completude = 0;
        }

    }

    /**
     * Recupération de l'employé désigné
     *
     * @return
     */
    public Employe getEmploye() {
        return employe;
    }

    /**
     * Mise à jour de l'employé
     *
     * @param employe
     */
    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    /**
     * Ne répond pas au cahier des charges mais permet d'avoir une lecture du
     * programme plus claire
     *
     * @return
     */
    @Override
    public String toString() {
        return "Tache{" + "description=" + description + ", duree=" + duree + ", completude=" + completude + ", employe=" + employe + '}';
    }

}
